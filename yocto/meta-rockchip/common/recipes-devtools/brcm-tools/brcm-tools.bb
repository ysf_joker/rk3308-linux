DESCRIPTION = "Broadcom develop tools"
SECTION = "devel"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"

SRC_URI = "file://dhd_priv.c \
	   file://raw.c \
	   file://brcm_patchram_plus1.c \
	   file://Makefile \
	  "

S = "${WORKDIR}"

do_compile_append() {
	${CC} ${CFLAGS} ${LDFLAGS} raw.c -o dhd_capture
	${CC} ${CFLAGS} ${LDFLAGS} brcm_patchram_plus1.c -o brcm_patchram_plus
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 dhd_priv ${D}${bindir}
	install -m 0755 dhd_capture ${D}${bindir}
	install -m 0755 brcm_patchram_plus ${D}${bindir}
}
